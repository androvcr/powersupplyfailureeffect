# PowerSupplyFailureEffect

Energy/Power supply failure effect that can be applied to any html element using javascript

This library is created with the idea to start a new project with a lot of funny, nice and well documentated/designed effects.

# How to install
We are writting the webpack lib in this moments, if you can't wait... then, download this repo, and use the ./dist/app.bundle.js file

# How to use it
Be sure that you already included the effect javascript file

## HTML File
```
<html>
  <head></head>
  <body>
    <h1 id="blinkingHeader">Hello there!</h1>
  <body>
</html>
```

## JS File
```
window.onload = function() {
  const energyManager = new PowerSupplyFailureEffect();
  // Init the power supply failure effect.
  energyManager.startFailureSupply('blinkingHeader');
};
```

# Working example!
Yes, it works!, take a look in here!


# What's next?
We are going to continue adding more features and options, stay tuned!


# How to create a webpack package with typescript?
https://github.com/nobrainr/typescript-webpack-starter