export declare class PowerSupplyFailureEffect {
    private _energyArray;
    constructor();
    private registerEnergyComponent;
    private stopFailureSupply;
    private getEnergyComponent;
    startFailureSupply(componentID: string, maxLight?: number): void;
}
