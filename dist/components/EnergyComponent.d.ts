export declare class EnergyComponent {
    private _id;
    private _htmlComponent;
    private _mainTimer;
    private _animate;
    private _maxLight;
    private _tempTimer;
    constructor(componentID: string, maxLight?: number);
    animate(): void;
    checkName(componentID: string): boolean;
    startFailureSupply(): void;
    private startFlashing;
    stopFailureSupply(): void;
    private setLight;
}
