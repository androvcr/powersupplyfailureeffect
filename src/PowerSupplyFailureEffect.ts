import { EnergyComponent } from './components/EnergyComponent';

export class PowerSupplyFailureEffect {

  // Private Varianles
  private _energyArray:Array<EnergyComponent> = [];

  // Main Constructor
  constructor() { }

  private registerEnergyComponent(componentID:string, maxLight:number): EnergyComponent {
  	let newComponent:EnergyComponent = new EnergyComponent(componentID, maxLight);
		this._energyArray.push(newComponent);
		return newComponent;
  }

	private stopFailureSupply(componentID:string):void {
		let temp = this.getEnergyComponent(componentID);
		if(temp){
			temp.stopFailureSupply();
		}
    return;
	}

	private getEnergyComponent(componentID:string):EnergyComponent|null {
		for(let i=0;i<this._energyArray.length;i++){
			let temp = this._energyArray[i];
			if(temp.checkName(componentID)){
				return temp;
			}
		}
		return null;
	}

  public startFailureSupply(componentID:string, maxLight:number=1):void {
    this.stopFailureSupply(componentID);
    let temp = this.getEnergyComponent(componentID);
    if(!temp){
      temp = this.registerEnergyComponent(componentID, maxLight);
    }
    temp.animate();
    temp.startFailureSupply();
    return;
  }
}