export class EnergyComponent {
  // Main Constructor
  private _id:string;
  private _htmlComponent:HTMLElement|null;
  private _mainTimer:number = 0;
  private _animate:boolean = false;
  private _maxLight:number = 1;
  private _tempTimer:number = 0;

  constructor(componentID:string, maxLight:number=1) { 
    this._id = componentID;
    this._maxLight = maxLight;
    this._htmlComponent = document.getElementById(componentID);
    console.debug(this._htmlComponent);
  }

  public animate():void {
    this._animate = true;
  }

  public checkName(componentID:string):boolean {
    return this._id === componentID;
  }

  public startFailureSupply():void {
    if(this._animate){
      let randomTimer = Math.random() * 8;
      randomTimer *= 1000;
      setTimeout(()=>{
        this.startFlashing();
      },randomTimer);
    }
  }

  private startFlashing():void {
    let temporalValue:number = Math.random() * 30;
    let randomCount:number = Number(5 + temporalValue);
    clearInterval(this._tempTimer);
    this._tempTimer = setInterval(()=>{
      if(randomCount<=0){
        clearInterval(this._tempTimer);
        this.setLight(this._maxLight);
        this.startFailureSupply();
      }else{
        this.setLight(Math.random());
      }
      randomCount--;
      console.log(randomCount);
    }, 20);
  }

  public stopFailureSupply():void {
    this._animate = false;
    clearInterval(this._mainTimer);
    clearInterval(this._tempTimer);
    this._mainTimer = 0;
    this._tempTimer = 0;
    this.setLight(this._maxLight);
  }

  private setLight(lightValue:Number):void {
    if(this._htmlComponent !== null) {
      this._htmlComponent.style.opacity = `${lightValue}`;
    }
  }

}

// var energyManager = EnergyManagerSingleton();
// energyManager.startFailureSuply('#wrapper-couples img',1);